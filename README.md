&nbsp;

# Slices with redux toolkit
&nbsp;Nesse pequeno projeto, fiz minha implementação do Redux Slices utilizando o Redux toolkit, o objetivo final era centralizar o estado da aplicação e tornar simples a integração com APIs externas.
&nbsp;
&nbsp;

<p align="center" margin-bottom="0">
  <a href="https://redux-toolkit.js.org/">
    <img alt="Stringfy" align="center" width="auto" height="auto" src="https://codeit.mk/dam/jcr:2200ee35-a3b1-43c0-9589-e036ae9ea5d8/redux.1.2020-12-17-07-49-19.png">
  </a>
</p>

&nbsp;
## Leitura para um melhor entendimento
[How To Setup Redux Slices with Redux Toolkit](https://www.softkraft.co/how-to-setup-slices-with-redux-toolkit)

&nbsp;
## 📦 Packages

- [axios](https://www.npmjs.com/package/axios)
- [lodash](https://www.npmjs.com/package/lodash)
- [redux](https://www.npmjs.com/package/redux)
- [react-redux](https://www.npmjs.com/package/react-redux)
- [redux-persist](https://www.npmjs.com/package/redux-persist)
- [@reduxjs/toolkit](https://www.npmjs.com/package/@reduxjs/toolkit)